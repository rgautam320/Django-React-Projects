# Django Reacts Projects

These projects were made by Rajan Gautam using Django, React and Bootstrap.

## Steps to Run the Application

1. Clone this Repository
2. Browse through the specific project
3. Check the folder structure.
4. If it has frontend and all other files in the folder itself, just run `python -m virtualenv env` then `env\Scripts\activate` and then `pip install -r requirements.txt`. Then run `python manage.py runserver`.
5. If it has frontend and backend folder, do the previous step for backend folder, it should work. The frontend folder is having build folder and the React app.

## List of Projects

1. Auth System
2. React Blog - [Live Link](https://rgautam320-blog-react.herokuapp.com)
3. ToDo - [Live Link](https://rgautam320-todo.herokuapp.com)

#### Thank You

#### Rajan Gautam
